import java.io.IOException;
import java.util.Scanner;

/**
 * Classe que guarda as variaveis compartilhadas, 
 * algumas variaveis precisam ser acessadas de maneira controlada,
 * para evitar a condicao de corrida.
 */
class sincroniza_highlife{
    private int alive;
    private int grid[][];
    private int newGrid[][];

    public int getGrid(int i, int j)
    {
        return grid[i][j];
    }

    public synchronized int getAlive()
    {
        return alive;
    }

    public synchronized void setAlive(int value)
    {
        alive += value;
    }

    public void setNewGrid(int i, int j, int value)
    {
        newGrid[i][j] = value;
    }

    public void swap(int i, int j)
    {
        grid[i][j] = newGrid[i][j];
    }

    public void create(int n)
    {
        grid = new int[n][n];
        newGrid = new int[n][n];
        int lin = 1, col = 1;
        grid[lin][col+1] = 1;
        grid[lin+1][col+2] = 1;
        grid[lin+2][col] = 1;
        grid[lin+2][col+1] = 1;
        grid[lin+2][col+2] = 1;

        //R-pentomino
        lin =10; col = 30;
        grid[lin][col+1] = 1;
        grid[lin][col+2] = 1;
        grid[lin+1][col] = 1;
        grid[lin+1][col+1] = 1;
        grid[lin+2][col+1] = 1;

        alive = 10;
    }

    public void printGrid()
    {
        int i;
        int j;
        for(i = 0; i < 50; i++)
        {
            for(j = 0; j < 50; j++)
            {
                System.out.printf("%d ", grid[i][j]);
            }
            System.out.printf("\n");
        }
    }
}


public class highlife implements Runnable{
    private static sincroniza_highlife sinc;
    private static Thread arrayThreads[];
    private int inicio;
    private int mode;
    private int fim;
    private static int n;
    private static int threads;
    private static int generations;
    public static void main(String[] args) throws IOException, InterruptedException{
        Scanner scan = new Scanner(System.in);
        int i, j, batch, rest;
        int p1 = 0;
        int p2 = 0;
        int print_grid;
        System.out.print("Insira as dimensoes do tabuleiro:");
        n = scan.nextInt();
        sinc = new sincroniza_highlife();
        sinc.create(n);
        
        System.out.print("Insira a quantidade de threads:");

        threads = scan.nextInt();

        //array de Threads para poder sincroniza-las ao final (metodo join())
        arrayThreads = new Thread[threads];

        //separa o tamanho da iteracao para cada Thread em batchs
        batch = ((n * n)  / threads);

        //guarda o resto que sobrar da divisao, sera executado pela ultima Thread
        rest = ((n * n) % threads);

        System.out.print("Insira quantas geracoes(iteracoes):");

        generations = scan.nextInt();
        
        System.out.print("Deseja exibir o tabuleiro a cada iteracao?\n");
        System.out.print("0 - nao\n");
        System.out.print("1 - sim\n");
        print_grid = scan.nextInt();

        long ti = System.currentTimeMillis();
        //Executa as geracoes
        for(i = 0; i < generations; i++)
        {
            //Determina o ponto de inicio (p1) e de fim (p2) da iteracao de cada Thread
            for(j = 0; j < threads; j++)
            {
                if(j == 0)
                {
                    if(threads == 1)
                    {
                        p1 = p2;
                        p2 = batch - 1;
                    }
                    else{
                        p1 = p2;
                        p2 = batch;
                    }
                }
                else if(j == (threads - 1))
                {
                    p1 = p2 + 1;
                    p2 = p2 + batch + rest - 1;
                }
                else{
                    p1 = p2 + 1;
                    p2 = p2 + batch;
                }
                //cria a Thread
                Thread T = new Thread(new jogo_da_vida(p1, p2, 0));
                //adiciona a Thread no array de controle
                addThread(j, T);
                //inicia a Thread
                getThread(j).start();
            }
            //Apos inicializar todas as Threads, espera todas encerrarem o processamento
            //Evitando avancar para a proxima geracao enquanto a geracao atual esta sendo processada
            for(j = 0; j < threads; j++)
            {
                getThread(j).join();
            }
            p1 = 0;
            p2 = 0;

            //realiza o mesmo procedimento, agora para atualizar as posicoes do novo tabuleiro
            for(j = 0; j < threads; j++)
            {
                if(j == 0)
                {
                    p1 = p2;
                    p2 = p1 + batch;
                }
                else if(j == (threads - 1))
                {
                    p1 = p2 + 1;
                    p2 = p2 + batch + rest - 1;
                }
                else{
                    p1 = p2 + 1;
                    p2 = p2 + batch;
                }
                Thread T = new Thread(new jogo_da_vida(p1, p2, 1));
                addThread(j, T);
                getThread(j).start();
            }
            for(j = 0; j < threads; j++)
            {
                getThread(j).join();
            }
            //fim da atualizacao

            p1 = 0;
            p2 = 0;
            System.out.printf("\n\n\n--------GEN %d : %d celulas vivas--------\n\n", i+1, sinc.getAlive());
            if(print_grid == 1)
            {
                sinc.printGrid();
            }
        }
        long tf = System.currentTimeMillis();

        System.out.println("Tempo de execucao:" + ((double)(tf - ti)/1000));
    }

    private static Thread getThread(int i)
    {
        return arrayThreads[i];
    }

    private static void addThread(int i, Thread T)
    {
        arrayThreads[i] = T;
    }

    public int getNeighbors(int i, int j)
    {
        //contador que guardará a quantidade de células vizinhas
        int cont = 0;

        //variáveis de apoio, caso a célula esteja na borda do tabuleiro
        int x1, y1, x2, y2;

    //verificações das extremidades do tabuleiro
        //1. se a célula está na borda direita do tabuleiro
        //2. se não 1, verifica se está na borda esquerda
        //3. posição da coluna ok
        if(j == (n-1))
        {
            x1 = j - 1;
            x2 = 0;
        }
        else if(j == 0)
        {
            x1 = n - 1;
            x2 = j + 1;
        }
        else
        {
            x1 = j - 1;
            x2 = j + 1;
        }

        //4. se a célula está na borda superior
        //5. se não 4, verifica se está na borda inferior
        //6. posição da linha ok
        if(i == 0)
        {
            y1 = n - 1;
            y2 = i + 1;
        }
        else if(i == (n-1))
        {
            y1 = i - 1;
            y2 = 0;
        }
        else
        {
            y1 = i - 1;
            y2 = i + 1;
        }
    // fim da verificação dos vizinhos da célula

    //conta quantos vizinhos estão vivos
        if(sinc.getGrid(y1, x1) == 1) cont++; // superior esquerdo
        if(sinc.getGrid(y1, x2) == 1) cont++; // superior direito
        if(sinc.getGrid(y1, j)  == 1) cont++; // superior central
        if(sinc.getGrid(y2, x1) == 1) cont++; // inferior esquerdo
        if(sinc.getGrid(y2, x2) == 1) cont++; // inferior direito
        if(sinc.getGrid(y2, j)  == 1) cont++; // inferior central
        if(sinc.getGrid(i, x1)  == 1) cont++; // central esquerdo
        if(sinc.getGrid(i, x2)  == 1) cont++; // central direito

        //System.out.printf("%d OK \n", cont);
    // retorna a quantidade de vizinhos vivos
        return cont;
    }

    /**
     * Construtor da Thread, armazena as variaveis privadas da Thread especifica
     * @param i -> inicio da iteracao
     * @param f -> fim da iteracao
     * @param funcao -> tipo de funcao que sera executada pela Thread
     */
    public highlife(int i, int f, int funcao)
    {
        mode = funcao;
        inicio = i;
        fim = f;
    }
    @Override
    public void run() {
        int i, x, y, vizinhos;
        // Mode = 0
        //Thread vai calcular a quantidade de vizinhos e ja determinar se a celula da proxima geracao
        //estara viva ou morta.
        if(mode == 0)
        {
            for(i = inicio; i <= fim; i++)
            {
                y = (i / n);
                x = (i % n);
                vizinhos = getNeighbors(y, x);
                if(sinc.getGrid(y, x) == 1)
                {
                    //1. tem menos que dois vizinhos = MORRE
                    //2. tem mais que 3 vizinhos = MORRE
                    //3. 2 ou 3 vizinhos = VIVE
                    if(vizinhos < 2)
                    {
                        sinc.setAlive(-1);
                        sinc.setNewGrid(y, x, 0);
                    }
                    else if(vizinhos > 3)
                    {
                        sinc.setAlive(-1);
                        sinc.setNewGrid(y, x, 0);
                    }
                    else
                    {
                        sinc.setNewGrid(y, x, 1);
                    }
                }
                else
                {
                    //1. se tem 3 vizinhos = REVIVE
                    //2. se tem 6 vizinhos = REVIVE (HighLife mode)
                    if(vizinhos == 3)
                    {
                        sinc.setAlive(1);
                        sinc.setNewGrid(y, x, 1);
                    }
                    if(vizinhos == 6)
                    {
                        sinc.setAlive(1);
                        sinc.setNewGrid(y, x, 1);
                    }
                }
            }
        }
        //Mode = 1
        //Realiza a troca de tabuleiro (grid = newGrid).
        if(mode == 1)
        {
            for(i = inicio; i < fim; i++)
            {
                //System.out.printf("\n%d OK\n", i);
                y = (i / n);
                x = (i % n);
                sinc.swap(y, x);
            }
        }
    }
}